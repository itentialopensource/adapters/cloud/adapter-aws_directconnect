/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-aws_directconnect',
      type: 'AwsDirectConnect',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const AwsDirectConnect = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] AwsDirectConnect Adapter Test', () => {
  describe('AwsDirectConnect Class Tests', () => {
    const a = new AwsDirectConnect(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-aws_directconnect-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-aws_directconnect-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const xAmzTargetOvertureServiceAcceptDirectConnectGatewayAssociationProposalAcceptDirectConnectGatewayAssociationProposalBodyParam = {
      associatedGatewayOwnerAccount: {},
      directConnectGatewayId: {},
      proposalId: {}
    };
    describe('#acceptDirectConnectGatewayAssociationProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.acceptDirectConnectGatewayAssociationProposal(xAmzTargetOvertureServiceAcceptDirectConnectGatewayAssociationProposalAcceptDirectConnectGatewayAssociationProposalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.directConnectGatewayAssociation);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceAcceptDirectConnectGatewayAssociationProposal', 'acceptDirectConnectGatewayAssociationProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceAllocateConnectionOnInterconnectAllocateConnectionOnInterconnectBodyParam = {
      bandwidth: {},
      connectionName: {},
      interconnectId: {},
      ownerAccount: {},
      vlan: {}
    };
    describe('#allocateConnectionOnInterconnect - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.allocateConnectionOnInterconnect(xAmzTargetOvertureServiceAllocateConnectionOnInterconnectAllocateConnectionOnInterconnectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.awsDevice);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bandwidth);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.connectionName);
                assert.equal('object', typeof data.response.connectionState);
                assert.equal('object', typeof data.response.hasLogicalRedundancy);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.lagId);
                assert.equal('object', typeof data.response.loaIssueTime);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.partnerName);
                assert.equal('object', typeof data.response.providerName);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceAllocateConnectionOnInterconnect', 'allocateConnectionOnInterconnect', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceAllocateHostedConnectionAllocateHostedConnectionBodyParam = {
      bandwidth: {},
      connectionId: {},
      connectionName: {},
      ownerAccount: {},
      vlan: {}
    };
    describe('#allocateHostedConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.allocateHostedConnection(xAmzTargetOvertureServiceAllocateHostedConnectionAllocateHostedConnectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.awsDevice);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bandwidth);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.connectionName);
                assert.equal('object', typeof data.response.connectionState);
                assert.equal('object', typeof data.response.hasLogicalRedundancy);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.lagId);
                assert.equal('object', typeof data.response.loaIssueTime);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.partnerName);
                assert.equal('object', typeof data.response.providerName);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceAllocateHostedConnection', 'allocateHostedConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceAllocatePrivateVirtualInterfaceAllocatePrivateVirtualInterfaceBodyParam = {
      connectionId: {},
      newPrivateVirtualInterfaceAllocation: {},
      ownerAccount: {}
    };
    describe('#allocatePrivateVirtualInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.allocatePrivateVirtualInterface(xAmzTargetOvertureServiceAllocatePrivateVirtualInterfaceAllocatePrivateVirtualInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.addressFamily);
                assert.equal('object', typeof data.response.amazonAddress);
                assert.equal('object', typeof data.response.amazonSideAsn);
                assert.equal('object', typeof data.response.asn);
                assert.equal('object', typeof data.response.authKey);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bgpPeers);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.customerAddress);
                assert.equal('object', typeof data.response.customerRouterConfig);
                assert.equal('object', typeof data.response.directConnectGatewayId);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.mtu);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.routeFilterPrefixes);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.virtualGatewayId);
                assert.equal('object', typeof data.response.virtualInterfaceId);
                assert.equal('object', typeof data.response.virtualInterfaceName);
                assert.equal('object', typeof data.response.virtualInterfaceState);
                assert.equal('object', typeof data.response.virtualInterfaceType);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceAllocatePrivateVirtualInterface', 'allocatePrivateVirtualInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceAllocatePublicVirtualInterfaceAllocatePublicVirtualInterfaceBodyParam = {
      connectionId: {},
      newPublicVirtualInterfaceAllocation: {},
      ownerAccount: {}
    };
    describe('#allocatePublicVirtualInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.allocatePublicVirtualInterface(xAmzTargetOvertureServiceAllocatePublicVirtualInterfaceAllocatePublicVirtualInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.addressFamily);
                assert.equal('object', typeof data.response.amazonAddress);
                assert.equal('object', typeof data.response.amazonSideAsn);
                assert.equal('object', typeof data.response.asn);
                assert.equal('object', typeof data.response.authKey);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bgpPeers);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.customerAddress);
                assert.equal('object', typeof data.response.customerRouterConfig);
                assert.equal('object', typeof data.response.directConnectGatewayId);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.mtu);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.routeFilterPrefixes);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.virtualGatewayId);
                assert.equal('object', typeof data.response.virtualInterfaceId);
                assert.equal('object', typeof data.response.virtualInterfaceName);
                assert.equal('object', typeof data.response.virtualInterfaceState);
                assert.equal('object', typeof data.response.virtualInterfaceType);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceAllocatePublicVirtualInterface', 'allocatePublicVirtualInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceAllocateTransitVirtualInterfaceAllocateTransitVirtualInterfaceBodyParam = {
      connectionId: {},
      newTransitVirtualInterfaceAllocation: {},
      ownerAccount: {}
    };
    describe('#allocateTransitVirtualInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.allocateTransitVirtualInterface(xAmzTargetOvertureServiceAllocateTransitVirtualInterfaceAllocateTransitVirtualInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.virtualInterface);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceAllocateTransitVirtualInterface', 'allocateTransitVirtualInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceAssociateConnectionWithLagAssociateConnectionWithLagBodyParam = {
      connectionId: {},
      lagId: {}
    };
    describe('#associateConnectionWithLag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associateConnectionWithLag(xAmzTargetOvertureServiceAssociateConnectionWithLagAssociateConnectionWithLagBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.awsDevice);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bandwidth);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.connectionName);
                assert.equal('object', typeof data.response.connectionState);
                assert.equal('object', typeof data.response.hasLogicalRedundancy);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.lagId);
                assert.equal('object', typeof data.response.loaIssueTime);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.partnerName);
                assert.equal('object', typeof data.response.providerName);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceAssociateConnectionWithLag', 'associateConnectionWithLag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceAssociateHostedConnectionAssociateHostedConnectionBodyParam = {
      connectionId: {},
      parentConnectionId: {}
    };
    describe('#associateHostedConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associateHostedConnection(xAmzTargetOvertureServiceAssociateHostedConnectionAssociateHostedConnectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.awsDevice);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bandwidth);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.connectionName);
                assert.equal('object', typeof data.response.connectionState);
                assert.equal('object', typeof data.response.hasLogicalRedundancy);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.lagId);
                assert.equal('object', typeof data.response.loaIssueTime);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.partnerName);
                assert.equal('object', typeof data.response.providerName);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceAssociateHostedConnection', 'associateHostedConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceAssociateVirtualInterfaceAssociateVirtualInterfaceBodyParam = {
      connectionId: {},
      virtualInterfaceId: {}
    };
    describe('#associateVirtualInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associateVirtualInterface(xAmzTargetOvertureServiceAssociateVirtualInterfaceAssociateVirtualInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.addressFamily);
                assert.equal('object', typeof data.response.amazonAddress);
                assert.equal('object', typeof data.response.amazonSideAsn);
                assert.equal('object', typeof data.response.asn);
                assert.equal('object', typeof data.response.authKey);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bgpPeers);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.customerAddress);
                assert.equal('object', typeof data.response.customerRouterConfig);
                assert.equal('object', typeof data.response.directConnectGatewayId);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.mtu);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.routeFilterPrefixes);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.virtualGatewayId);
                assert.equal('object', typeof data.response.virtualInterfaceId);
                assert.equal('object', typeof data.response.virtualInterfaceName);
                assert.equal('object', typeof data.response.virtualInterfaceState);
                assert.equal('object', typeof data.response.virtualInterfaceType);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceAssociateVirtualInterface', 'associateVirtualInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceConfirmConnectionConfirmConnectionBodyParam = {
      connectionId: {}
    };
    describe('#confirmConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.confirmConnection(xAmzTargetOvertureServiceConfirmConnectionConfirmConnectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.connectionState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceConfirmConnection', 'confirmConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceConfirmPrivateVirtualInterfaceConfirmPrivateVirtualInterfaceBodyParam = {
      virtualInterfaceId: {}
    };
    describe('#confirmPrivateVirtualInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.confirmPrivateVirtualInterface(xAmzTargetOvertureServiceConfirmPrivateVirtualInterfaceConfirmPrivateVirtualInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.virtualInterfaceState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceConfirmPrivateVirtualInterface', 'confirmPrivateVirtualInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceConfirmPublicVirtualInterfaceConfirmPublicVirtualInterfaceBodyParam = {
      virtualInterfaceId: {}
    };
    describe('#confirmPublicVirtualInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.confirmPublicVirtualInterface(xAmzTargetOvertureServiceConfirmPublicVirtualInterfaceConfirmPublicVirtualInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.virtualInterfaceState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceConfirmPublicVirtualInterface', 'confirmPublicVirtualInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceConfirmTransitVirtualInterfaceConfirmTransitVirtualInterfaceBodyParam = {
      directConnectGatewayId: {},
      virtualInterfaceId: {}
    };
    describe('#confirmTransitVirtualInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.confirmTransitVirtualInterface(xAmzTargetOvertureServiceConfirmTransitVirtualInterfaceConfirmTransitVirtualInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.virtualInterfaceState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceConfirmTransitVirtualInterface', 'confirmTransitVirtualInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceCreateBGPPeerCreateBGPPeerBodyParam = {
      newBGPPeer: {
        addressFamily: {},
        amazonAddress: {},
        asn: {},
        authKey: {},
        customerAddress: {}
      },
      virtualInterfaceId: {}
    };
    describe('#createBGPPeer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createBGPPeer(xAmzTargetOvertureServiceCreateBGPPeerCreateBGPPeerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.virtualInterface);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceCreateBGPPeer', 'createBGPPeer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceCreateConnectionCreateConnectionBodyParam = {
      bandwidth: {},
      connectionName: {},
      location: {}
    };
    describe('#createConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createConnection(xAmzTargetOvertureServiceCreateConnectionCreateConnectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.awsDevice);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bandwidth);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.connectionName);
                assert.equal('object', typeof data.response.connectionState);
                assert.equal('object', typeof data.response.hasLogicalRedundancy);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.lagId);
                assert.equal('object', typeof data.response.loaIssueTime);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.partnerName);
                assert.equal('object', typeof data.response.providerName);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceCreateConnection', 'createConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceCreateDirectConnectGatewayCreateDirectConnectGatewayBodyParam = {
      directConnectGatewayName: {}
    };
    describe('#createDirectConnectGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDirectConnectGateway(xAmzTargetOvertureServiceCreateDirectConnectGatewayCreateDirectConnectGatewayBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.directConnectGateway);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceCreateDirectConnectGateway', 'createDirectConnectGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceCreateDirectConnectGatewayAssociationCreateDirectConnectGatewayAssociationBodyParam = {
      directConnectGatewayId: {}
    };
    describe('#createDirectConnectGatewayAssociation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDirectConnectGatewayAssociation(xAmzTargetOvertureServiceCreateDirectConnectGatewayAssociationCreateDirectConnectGatewayAssociationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.directConnectGatewayAssociation);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceCreateDirectConnectGatewayAssociation', 'createDirectConnectGatewayAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceCreateDirectConnectGatewayAssociationProposalCreateDirectConnectGatewayAssociationProposalBodyParam = {
      directConnectGatewayId: {},
      directConnectGatewayOwnerAccount: {},
      gatewayId: {}
    };
    describe('#createDirectConnectGatewayAssociationProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDirectConnectGatewayAssociationProposal(xAmzTargetOvertureServiceCreateDirectConnectGatewayAssociationProposalCreateDirectConnectGatewayAssociationProposalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.directConnectGatewayAssociationProposal);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceCreateDirectConnectGatewayAssociationProposal', 'createDirectConnectGatewayAssociationProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceCreateInterconnectCreateInterconnectBodyParam = {
      bandwidth: {},
      interconnectName: {},
      location: {}
    };
    describe('#createInterconnect - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createInterconnect(xAmzTargetOvertureServiceCreateInterconnectCreateInterconnectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.awsDevice);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bandwidth);
                assert.equal('object', typeof data.response.hasLogicalRedundancy);
                assert.equal('object', typeof data.response.interconnectId);
                assert.equal('object', typeof data.response.interconnectName);
                assert.equal('object', typeof data.response.interconnectState);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.lagId);
                assert.equal('object', typeof data.response.loaIssueTime);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.providerName);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceCreateInterconnect', 'createInterconnect', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceCreateLagCreateLagBodyParam = {
      connectionsBandwidth: {},
      lagName: {},
      location: {},
      numberOfConnections: {}
    };
    describe('#createLag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createLag(xAmzTargetOvertureServiceCreateLagCreateLagBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.allowsHostedConnections);
                assert.equal('object', typeof data.response.awsDevice);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.connections);
                assert.equal('object', typeof data.response.connectionsBandwidth);
                assert.equal('object', typeof data.response.hasLogicalRedundancy);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.lagId);
                assert.equal('object', typeof data.response.lagName);
                assert.equal('object', typeof data.response.lagState);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.minimumLinks);
                assert.equal('object', typeof data.response.numberOfConnections);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.providerName);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceCreateLag', 'createLag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceCreatePrivateVirtualInterfaceCreatePrivateVirtualInterfaceBodyParam = {
      connectionId: {},
      newPrivateVirtualInterface: {}
    };
    describe('#createPrivateVirtualInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPrivateVirtualInterface(xAmzTargetOvertureServiceCreatePrivateVirtualInterfaceCreatePrivateVirtualInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.addressFamily);
                assert.equal('object', typeof data.response.amazonAddress);
                assert.equal('object', typeof data.response.amazonSideAsn);
                assert.equal('object', typeof data.response.asn);
                assert.equal('object', typeof data.response.authKey);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bgpPeers);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.customerAddress);
                assert.equal('object', typeof data.response.customerRouterConfig);
                assert.equal('object', typeof data.response.directConnectGatewayId);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.mtu);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.routeFilterPrefixes);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.virtualGatewayId);
                assert.equal('object', typeof data.response.virtualInterfaceId);
                assert.equal('object', typeof data.response.virtualInterfaceName);
                assert.equal('object', typeof data.response.virtualInterfaceState);
                assert.equal('object', typeof data.response.virtualInterfaceType);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceCreatePrivateVirtualInterface', 'createPrivateVirtualInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceCreatePublicVirtualInterfaceCreatePublicVirtualInterfaceBodyParam = {
      connectionId: {},
      newPublicVirtualInterface: {}
    };
    describe('#createPublicVirtualInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPublicVirtualInterface(xAmzTargetOvertureServiceCreatePublicVirtualInterfaceCreatePublicVirtualInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.addressFamily);
                assert.equal('object', typeof data.response.amazonAddress);
                assert.equal('object', typeof data.response.amazonSideAsn);
                assert.equal('object', typeof data.response.asn);
                assert.equal('object', typeof data.response.authKey);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bgpPeers);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.customerAddress);
                assert.equal('object', typeof data.response.customerRouterConfig);
                assert.equal('object', typeof data.response.directConnectGatewayId);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.mtu);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.routeFilterPrefixes);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.virtualGatewayId);
                assert.equal('object', typeof data.response.virtualInterfaceId);
                assert.equal('object', typeof data.response.virtualInterfaceName);
                assert.equal('object', typeof data.response.virtualInterfaceState);
                assert.equal('object', typeof data.response.virtualInterfaceType);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceCreatePublicVirtualInterface', 'createPublicVirtualInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceCreateTransitVirtualInterfaceCreateTransitVirtualInterfaceBodyParam = {
      connectionId: {},
      newTransitVirtualInterface: {}
    };
    describe('#createTransitVirtualInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTransitVirtualInterface(xAmzTargetOvertureServiceCreateTransitVirtualInterfaceCreateTransitVirtualInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.virtualInterface);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceCreateTransitVirtualInterface', 'createTransitVirtualInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDeleteBGPPeerDeleteBGPPeerBodyParam = {
      asn: {},
      bgpPeerId: {},
      customerAddress: {},
      virtualInterfaceId: {}
    };
    describe('#deleteBGPPeer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteBGPPeer(xAmzTargetOvertureServiceDeleteBGPPeerDeleteBGPPeerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.virtualInterface);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDeleteBGPPeer', 'deleteBGPPeer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDeleteConnectionDeleteConnectionBodyParam = {
      connectionId: {}
    };
    describe('#deleteConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteConnection(xAmzTargetOvertureServiceDeleteConnectionDeleteConnectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.awsDevice);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bandwidth);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.connectionName);
                assert.equal('object', typeof data.response.connectionState);
                assert.equal('object', typeof data.response.hasLogicalRedundancy);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.lagId);
                assert.equal('object', typeof data.response.loaIssueTime);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.partnerName);
                assert.equal('object', typeof data.response.providerName);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDeleteConnection', 'deleteConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDeleteDirectConnectGatewayDeleteDirectConnectGatewayBodyParam = {
      directConnectGatewayId: {}
    };
    describe('#deleteDirectConnectGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDirectConnectGateway(xAmzTargetOvertureServiceDeleteDirectConnectGatewayDeleteDirectConnectGatewayBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.directConnectGateway);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDeleteDirectConnectGateway', 'deleteDirectConnectGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDeleteDirectConnectGatewayAssociationDeleteDirectConnectGatewayAssociationBodyParam = {
      associationId: {},
      directConnectGatewayId: {},
      virtualGatewayId: {}
    };
    describe('#deleteDirectConnectGatewayAssociation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDirectConnectGatewayAssociation(xAmzTargetOvertureServiceDeleteDirectConnectGatewayAssociationDeleteDirectConnectGatewayAssociationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.directConnectGatewayAssociation);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDeleteDirectConnectGatewayAssociation', 'deleteDirectConnectGatewayAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDeleteDirectConnectGatewayAssociationProposalDeleteDirectConnectGatewayAssociationProposalBodyParam = {
      proposalId: {}
    };
    describe('#deleteDirectConnectGatewayAssociationProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDirectConnectGatewayAssociationProposal(xAmzTargetOvertureServiceDeleteDirectConnectGatewayAssociationProposalDeleteDirectConnectGatewayAssociationProposalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.directConnectGatewayAssociationProposal);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDeleteDirectConnectGatewayAssociationProposal', 'deleteDirectConnectGatewayAssociationProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDeleteInterconnectDeleteInterconnectBodyParam = {
      interconnectId: {}
    };
    describe('#deleteInterconnect - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteInterconnect(xAmzTargetOvertureServiceDeleteInterconnectDeleteInterconnectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.interconnectState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDeleteInterconnect', 'deleteInterconnect', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDeleteLagDeleteLagBodyParam = {
      lagId: {}
    };
    describe('#deleteLag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteLag(xAmzTargetOvertureServiceDeleteLagDeleteLagBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.allowsHostedConnections);
                assert.equal('object', typeof data.response.awsDevice);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.connections);
                assert.equal('object', typeof data.response.connectionsBandwidth);
                assert.equal('object', typeof data.response.hasLogicalRedundancy);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.lagId);
                assert.equal('object', typeof data.response.lagName);
                assert.equal('object', typeof data.response.lagState);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.minimumLinks);
                assert.equal('object', typeof data.response.numberOfConnections);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.providerName);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDeleteLag', 'deleteLag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDeleteVirtualInterfaceDeleteVirtualInterfaceBodyParam = {
      virtualInterfaceId: {}
    };
    describe('#deleteVirtualInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVirtualInterface(xAmzTargetOvertureServiceDeleteVirtualInterfaceDeleteVirtualInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.virtualInterfaceState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDeleteVirtualInterface', 'deleteVirtualInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeConnectionLoaDescribeConnectionLoaBodyParam = {
      connectionId: {}
    };
    describe('#describeConnectionLoa - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeConnectionLoa(xAmzTargetOvertureServiceDescribeConnectionLoaDescribeConnectionLoaBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.loa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeConnectionLoa', 'describeConnectionLoa', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeConnectionsDescribeConnectionsBodyParam = {
      connectionId: {}
    };
    describe('#describeConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeConnections(xAmzTargetOvertureServiceDescribeConnectionsDescribeConnectionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.connections);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeConnections', 'describeConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeConnectionsOnInterconnectDescribeConnectionsOnInterconnectBodyParam = {
      interconnectId: {}
    };
    describe('#describeConnectionsOnInterconnect - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeConnectionsOnInterconnect(xAmzTargetOvertureServiceDescribeConnectionsOnInterconnectDescribeConnectionsOnInterconnectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.connections);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeConnectionsOnInterconnect', 'describeConnectionsOnInterconnect', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeDirectConnectGatewayAssociationProposalsDescribeDirectConnectGatewayAssociationProposalsBodyParam = {
      associatedGatewayId: {},
      directConnectGatewayId: {},
      maxResults: {},
      nextToken: {},
      proposalId: {}
    };
    describe('#describeDirectConnectGatewayAssociationProposals - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeDirectConnectGatewayAssociationProposals(xAmzTargetOvertureServiceDescribeDirectConnectGatewayAssociationProposalsDescribeDirectConnectGatewayAssociationProposalsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.directConnectGatewayAssociationProposals);
                assert.equal('object', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeDirectConnectGatewayAssociationProposals', 'describeDirectConnectGatewayAssociationProposals', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeDirectConnectGatewayAssociationsDescribeDirectConnectGatewayAssociationsBodyParam = {
      associatedGatewayId: {},
      associationId: {},
      directConnectGatewayId: {},
      maxResults: {},
      nextToken: {},
      virtualGatewayId: {}
    };
    describe('#describeDirectConnectGatewayAssociations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeDirectConnectGatewayAssociations(xAmzTargetOvertureServiceDescribeDirectConnectGatewayAssociationsDescribeDirectConnectGatewayAssociationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.directConnectGatewayAssociations);
                assert.equal('object', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeDirectConnectGatewayAssociations', 'describeDirectConnectGatewayAssociations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeDirectConnectGatewayAttachmentsDescribeDirectConnectGatewayAttachmentsBodyParam = {
      directConnectGatewayId: {},
      maxResults: {},
      nextToken: {},
      virtualInterfaceId: {}
    };
    describe('#describeDirectConnectGatewayAttachments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeDirectConnectGatewayAttachments(xAmzTargetOvertureServiceDescribeDirectConnectGatewayAttachmentsDescribeDirectConnectGatewayAttachmentsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.directConnectGatewayAttachments);
                assert.equal('object', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeDirectConnectGatewayAttachments', 'describeDirectConnectGatewayAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeDirectConnectGatewaysDescribeDirectConnectGatewaysBodyParam = {
      directConnectGatewayId: {},
      maxResults: {},
      nextToken: {}
    };
    describe('#describeDirectConnectGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeDirectConnectGateways(xAmzTargetOvertureServiceDescribeDirectConnectGatewaysDescribeDirectConnectGatewaysBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.directConnectGateways);
                assert.equal('object', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeDirectConnectGateways', 'describeDirectConnectGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeHostedConnectionsDescribeHostedConnectionsBodyParam = {
      connectionId: {}
    };
    describe('#describeHostedConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeHostedConnections(xAmzTargetOvertureServiceDescribeHostedConnectionsDescribeHostedConnectionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.connections);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeHostedConnections', 'describeHostedConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeInterconnectLoaDescribeInterconnectLoaBodyParam = {
      interconnectId: {}
    };
    describe('#describeInterconnectLoa - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeInterconnectLoa(xAmzTargetOvertureServiceDescribeInterconnectLoaDescribeInterconnectLoaBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.loa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeInterconnectLoa', 'describeInterconnectLoa', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeInterconnectsDescribeInterconnectsBodyParam = {
      interconnectId: {}
    };
    describe('#describeInterconnects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeInterconnects(xAmzTargetOvertureServiceDescribeInterconnectsDescribeInterconnectsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.interconnects);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeInterconnects', 'describeInterconnects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeLagsDescribeLagsBodyParam = {
      lagId: {}
    };
    describe('#describeLags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeLags(xAmzTargetOvertureServiceDescribeLagsDescribeLagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.lags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeLags', 'describeLags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeLoaDescribeLoaBodyParam = {
      connectionId: {}
    };
    describe('#describeLoa - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeLoa(xAmzTargetOvertureServiceDescribeLoaDescribeLoaBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.loaContent);
                assert.equal('object', typeof data.response.loaContentType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeLoa', 'describeLoa', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeLocations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeLocations((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.locations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeLocations', 'describeLocations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeTagsDescribeTagsBodyParam = {
      resourceArns: {}
    };
    describe('#describeTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeTags(xAmzTargetOvertureServiceDescribeTagsDescribeTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resourceTags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeTags', 'describeTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVirtualGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVirtualGateways((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.virtualGateways);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeVirtualGateways', 'describeVirtualGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDescribeVirtualInterfacesDescribeVirtualInterfacesBodyParam = {
      connectionId: {},
      virtualInterfaceId: {}
    };
    describe('#describeVirtualInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVirtualInterfaces(xAmzTargetOvertureServiceDescribeVirtualInterfacesDescribeVirtualInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.virtualInterfaces);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDescribeVirtualInterfaces', 'describeVirtualInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceDisassociateConnectionFromLagDisassociateConnectionFromLagBodyParam = {
      connectionId: {},
      lagId: {}
    };
    describe('#disassociateConnectionFromLag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disassociateConnectionFromLag(xAmzTargetOvertureServiceDisassociateConnectionFromLagDisassociateConnectionFromLagBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.awsDevice);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bandwidth);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.connectionName);
                assert.equal('object', typeof data.response.connectionState);
                assert.equal('object', typeof data.response.hasLogicalRedundancy);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.lagId);
                assert.equal('object', typeof data.response.loaIssueTime);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.partnerName);
                assert.equal('object', typeof data.response.providerName);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceDisassociateConnectionFromLag', 'disassociateConnectionFromLag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceListVirtualInterfaceTestHistoryListVirtualInterfaceTestHistoryBodyParam = {
      bgpPeers: {},
      maxResults: {},
      nextToken: {},
      status: {},
      testId: {},
      virtualInterfaceId: {}
    };
    describe('#listVirtualInterfaceTestHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listVirtualInterfaceTestHistory(xAmzTargetOvertureServiceListVirtualInterfaceTestHistoryListVirtualInterfaceTestHistoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.nextToken);
                assert.equal('object', typeof data.response.virtualInterfaceTestHistory);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceListVirtualInterfaceTestHistory', 'listVirtualInterfaceTestHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceStartBgpFailoverTestStartBgpFailoverTestBodyParam = {
      virtualInterfaceId: {}
    };
    describe('#startBgpFailoverTest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.startBgpFailoverTest(xAmzTargetOvertureServiceStartBgpFailoverTestStartBgpFailoverTestBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.virtualInterfaceTest);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceStartBgpFailoverTest', 'startBgpFailoverTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceStopBgpFailoverTestStopBgpFailoverTestBodyParam = {
      virtualInterfaceId: {}
    };
    describe('#stopBgpFailoverTest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.stopBgpFailoverTest(xAmzTargetOvertureServiceStopBgpFailoverTestStopBgpFailoverTestBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.virtualInterfaceTest);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceStopBgpFailoverTest', 'stopBgpFailoverTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceTagResourceTagResourceBodyParam = {
      resourceArn: {},
      tags: {}
    };
    describe('#tagResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tagResource(xAmzTargetOvertureServiceTagResourceTagResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_directconnect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceTagResource', 'tagResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceUntagResourceUntagResourceBodyParam = {
      resourceArn: {},
      tagKeys: {}
    };
    describe('#untagResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.untagResource(xAmzTargetOvertureServiceUntagResourceUntagResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_directconnect-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceUntagResource', 'untagResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceUpdateDirectConnectGatewayAssociationUpdateDirectConnectGatewayAssociationBodyParam = {
      addAllowedPrefixesToDirectConnectGateway: {},
      associationId: {},
      removeAllowedPrefixesToDirectConnectGateway: {}
    };
    describe('#updateDirectConnectGatewayAssociation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDirectConnectGatewayAssociation(xAmzTargetOvertureServiceUpdateDirectConnectGatewayAssociationUpdateDirectConnectGatewayAssociationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.directConnectGatewayAssociation);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceUpdateDirectConnectGatewayAssociation', 'updateDirectConnectGatewayAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceUpdateLagUpdateLagBodyParam = {
      lagId: {}
    };
    describe('#updateLag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateLag(xAmzTargetOvertureServiceUpdateLagUpdateLagBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.allowsHostedConnections);
                assert.equal('object', typeof data.response.awsDevice);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.connections);
                assert.equal('object', typeof data.response.connectionsBandwidth);
                assert.equal('object', typeof data.response.hasLogicalRedundancy);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.lagId);
                assert.equal('object', typeof data.response.lagName);
                assert.equal('object', typeof data.response.lagState);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.minimumLinks);
                assert.equal('object', typeof data.response.numberOfConnections);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.providerName);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceUpdateLag', 'updateLag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const xAmzTargetOvertureServiceUpdateVirtualInterfaceAttributesUpdateVirtualInterfaceAttributesBodyParam = {
      virtualInterfaceId: {}
    };
    describe('#updateVirtualInterfaceAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVirtualInterfaceAttributes(xAmzTargetOvertureServiceUpdateVirtualInterfaceAttributesUpdateVirtualInterfaceAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.addressFamily);
                assert.equal('object', typeof data.response.amazonAddress);
                assert.equal('object', typeof data.response.amazonSideAsn);
                assert.equal('object', typeof data.response.asn);
                assert.equal('object', typeof data.response.authKey);
                assert.equal('object', typeof data.response.awsDeviceV2);
                assert.equal('object', typeof data.response.bgpPeers);
                assert.equal('object', typeof data.response.connectionId);
                assert.equal('object', typeof data.response.customerAddress);
                assert.equal('object', typeof data.response.customerRouterConfig);
                assert.equal('object', typeof data.response.directConnectGatewayId);
                assert.equal('object', typeof data.response.jumboFrameCapable);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.mtu);
                assert.equal('object', typeof data.response.ownerAccount);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.routeFilterPrefixes);
                assert.equal('object', typeof data.response.tags);
                assert.equal('object', typeof data.response.virtualGatewayId);
                assert.equal('object', typeof data.response.virtualInterfaceId);
                assert.equal('object', typeof data.response.virtualInterfaceName);
                assert.equal('object', typeof data.response.virtualInterfaceState);
                assert.equal('object', typeof data.response.virtualInterfaceType);
                assert.equal('object', typeof data.response.vlan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('XAmzTargetOvertureServiceUpdateVirtualInterfaceAttributes', 'updateVirtualInterfaceAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
