## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for AWS DirectConnect. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for AWS DirectConnect.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Amazon AWS Direct Connect. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls. Note that each call in the DirectConnect API has the same path, and the action to be performed is identified in the `X-Amz-Target` header.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">acceptDirectConnectGatewayAssociationProposalSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Accepts a proposal request to attach a virtual private gateway or transit gateway to a Direct Conne</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allocateConnectionOnInterconnectSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deprecated. Use  AllocateHostedConnection  instead.   Creates a hosted connection on an interconnec</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allocateHostedConnectionSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a hosted connection on the specified interconnect or a link aggregation group (LAG) of inte</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allocatePrivateVirtualInterfaceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provisions a private virtual interface to be owned by the specified AWS account.   Virtual interfac</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allocatePublicVirtualInterfaceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provisions a public virtual interface to be owned by the specified AWS account.   The owner of a co</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allocateTransitVirtualInterfaceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provisions a transit virtual interface to be owned by the specified AWS account. Use this type of i</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateConnectionWithLagSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates an existing connection with a link aggregation group (LAG). The connection is interrupte</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateHostedConnectionSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates a hosted connection and its virtual interfaces with a link aggregation group (LAG) or in</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateVirtualInterfaceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates a virtual interface with a specified link aggregation group (LAG) or connection. Connect</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmConnectionSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Confirms the creation of the specified hosted connection on an interconnect.   Upon creation, the h</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmPrivateVirtualInterfaceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Accepts ownership of a private virtual interface created by another AWS account.   After the virtua</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmPublicVirtualInterfaceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Accepts ownership of a public virtual interface created by another AWS account.   After the virtual</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmTransitVirtualInterfaceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Accepts ownership of a transit virtual interface created by another AWS account.    After the owner</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBGPPeerSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a BGP peer on the specified virtual interface.   You must create a BGP peer for the corresp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConnectionSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a connection between a customer network and a specific AWS Direct Connect location.   A con</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDirectConnectGatewaySTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a Direct Connect gateway, which is an intermediate object that enables you to connect a set</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDirectConnectGatewayAssociationSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an association between a Direct Connect gateway and a virtual private gateway. The virtual</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDirectConnectGatewayAssociationProposalSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a proposal to associate the specified virtual private gateway or transit gateway with the s</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInterconnectSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an interconnect between an AWS Direct Connect Partner's network and a specific AWS Direct C</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLagSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a link aggregation group (LAG) with the specified number of bundled physical dedicated conn</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPrivateVirtualInterfaceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a private virtual interface. A virtual interface is the VLAN that transports AWS Direct Con</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPublicVirtualInterfaceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a public virtual interface. A virtual interface is the VLAN that transports AWS Direct Conn</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTransitVirtualInterfaceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a transit virtual interface. A transit virtual interface should be used to access one or mo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBGPPeerSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified BGP peer on the specified virtual interface with the specified customer addre</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConnectionSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified connection.   Deleting a connection only stops the AWS Direct Connect port ho</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDirectConnectGatewaySTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified Direct Connect gateway. You must first delete all virtual interfaces that are</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDirectConnectGatewayAssociationSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the association between the specified Direct Connect gateway and virtual private gateway.</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDirectConnectGatewayAssociationProposalSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the association proposal request between the specified Direct Connect gateway and virtual p</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInterconnectSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified interconnect.     Intended for use by AWS Direct Connect Partners only.</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLagSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified link aggregation group (LAG). You cannot delete a LAG if it has active virtua</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualInterfaceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a virtual interface.</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeConnectionLoaSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deprecated. Use  DescribeLoa  instead.   Gets the LOA-CFA for a connection.   The Letter of Authori</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeConnectionsSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Displays the specified connection or all connections in this Region.</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeConnectionsOnInterconnectSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deprecated. Use  DescribeHostedConnections  instead.   Lists the connections that have been provisi</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeDirectConnectGatewayAssociationProposalsSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more association proposals for connection between a virtual private gateway or tra</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeDirectConnectGatewayAssociationsSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the associations between your Direct Connect gateways and virtual private gateways. You must</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeDirectConnectGatewayAttachmentsSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the attachments between your Direct Connect gateways and virtual interfaces. You must specify</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeDirectConnectGatewaysSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists all your Direct Connect gateways or only the specified Direct Connect gateway. Deleted Direct</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeHostedConnectionsSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the hosted connections that have been provisioned on the specified interconnect or link aggre</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeInterconnectLoaSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deprecated. Use  DescribeLoa  instead.   Gets the LOA-CFA for the specified interconnect.   The Let</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeInterconnectsSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the interconnects owned by the AWS account or only the specified interconnect.</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeLagsSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes all your link aggregation groups (LAG) or the specified LAG.</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeLoaSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the LOA-CFA for a connection, interconnect, or link aggregation group (LAG).   The Letter of A</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeLocationsSTSRole(stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the AWS Direct Connect locations in the current AWS Region. These are the locations that can</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeTagsSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the tags associated with the specified AWS Direct Connect resources.</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVirtualGatewaysSTSRole(stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the virtual private gateways owned by the AWS account.   You can create one or more AWS Direc</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVirtualInterfacesSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Displays all virtual interfaces for an AWS account. Virtual interfaces deleted fewer than 15 minute</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateConnectionFromLagSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disassociates a connection from a link aggregation group (LAG). The connection is interrupted and r</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVirtualInterfaceTestHistorySTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the virtual interface failover test history.</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startBgpFailoverTestSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Starts the virtual interface failover test that verifies your configuration meets your resiliency r</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopBgpFailoverTestSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Stops the virtual interface failover test.</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tagResourceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Adds the specified tags to the specified AWS Direct Connect resource. Each resource can have a maxi</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">untagResourceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Removes one or more tags from the specified AWS Direct Connect resource.</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDirectConnectGatewayAssociationSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the specified attributes of the Direct Connect gateway association.   Add or remove prefixe</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLagSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the attributes of the specified link aggregation group (LAG).   You can update the followin</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVirtualInterfaceAttributesSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the specified attributes of the specified virtual private interface.   Setting the MTU of a</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
