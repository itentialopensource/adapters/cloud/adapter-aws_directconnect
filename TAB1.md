# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the AwsDirectConnect System. The API that was used to build the adapter for AwsDirectConnect is usually available in the report directory of this adapter. The adapter utilizes the AwsDirectConnect API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The AWS Direct Connect adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS Direct Connect.

With this adapter you have the ability to perform operations on items such as:

- Create Connection
- Describe Connection
- Create BGP Peer
- Create Interconnect
- Delete Interconnect
- Describe Locations
- Describe Virtual Interface

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
