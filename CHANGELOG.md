
## 0.5.8 [11-11-2024]

* more auth changes

See merge request itentialopensource/adapters/adapter-aws_directconnect!24

---

## 0.5.7 [10-15-2024]

* Changes made at 2024.10.14_20:49PM

See merge request itentialopensource/adapters/adapter-aws_directconnect!23

---

## 0.5.6 [09-30-2024]

* update auth docs

See merge request itentialopensource/adapters/adapter-aws_directconnect!21

---

## 0.5.5 [09-12-2024]

* add properties for sts

See merge request itentialopensource/adapters/adapter-aws_directconnect!20

---

## 0.5.4 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-aws_directconnect!19

---

## 0.5.3 [08-14-2024]

* Changes made at 2024.08.14_19:06PM

See merge request itentialopensource/adapters/adapter-aws_directconnect!18

---

## 0.5.2 [08-07-2024]

* Changes made at 2024.08.06_20:21PM

See merge request itentialopensource/adapters/adapter-aws_directconnect!17

---

## 0.5.1 [08-06-2024]

* Changes made at 2024.08.06_15:32PM

See merge request itentialopensource/adapters/adapter-aws_directconnect!16

---

## 0.5.0 [07-08-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-aws_directconnect!15

---

## 0.4.8 [03-28-2024]

* Changes made at 2024.03.28_13:22PM

See merge request itentialopensource/adapters/cloud/adapter-aws_directconnect!14

---

## 0.4.7 [03-21-2024]

* Changes made at 2024.03.21_13:56PM

See merge request itentialopensource/adapters/cloud/adapter-aws_directconnect!13

---

## 0.4.6 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-aws_directconnect!12

---

## 0.4.5 [03-13-2024]

* Changes made at 2024.03.13_11:46AM

See merge request itentialopensource/adapters/cloud/adapter-aws_directconnect!11

---

## 0.4.4 [03-11-2024]

* Changes made at 2024.03.11_15:35PM

See merge request itentialopensource/adapters/cloud/adapter-aws_directconnect!10

---

## 0.4.3 [02-28-2024]

* Changes made at 2024.02.28_11:50AM

See merge request itentialopensource/adapters/cloud/adapter-aws_directconnect!9

---

## 0.4.2 [01-27-2024]

* Patch/adapt 3205

See merge request itentialopensource/adapters/cloud/adapter-aws_directconnect!8

---

## 0.4.1 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-aws_directconnect!7

---

## 0.4.0 [12-14-2023]

* 2023 Adapter Migration

See merge request itentialopensource/adapters/cloud/adapter-aws_directconnect!6

---

## 0.3.0 [11-20-2023]

* 2023 Adapter Migration

See merge request itentialopensource/adapters/cloud/adapter-aws_directconnect!6

---
