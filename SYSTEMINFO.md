# AWS Direct Connect

Vendor: Amazon Web Services (AWS)
Homepage: https://aws.amazon.com/

Product: Direct Connect
Product Page: https://aws.amazon.com/directconnect/

## Introduction
We classify AWS Direct Connect into the Cloud domain as it provides a solution for direct connection to AWS Cloud Services. 

## Why Integrate
The AWS Direct Connect adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS Direct Connect.

With this adapter you have the ability to perform operations on items such as:

- Create Connection
- Describe Connection
- Create BGP Peer
- Create Interconnect
- Delete Interconnect
- Describe Locations
- Describe Virtual Interface

## Additional Product Documentation
The [API documents for AWS Direct Connect](https://docs.aws.amazon.com/directconnect/latest/APIReference/Welcome.html)
